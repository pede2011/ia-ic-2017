﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FSM<TS, TE>
{
    Dictionary<TS, Dictionary<TE, TS>> matrix = new Dictionary<TS, Dictionary<TE, TS>>();
    TS currentState;

    public void SetRelation(TS sourceState, TE evnt, TS destState)
    {
        if (!matrix.ContainsKey(sourceState))
        {
            matrix[sourceState] = new Dictionary<TE, TS>();
        }
        matrix[sourceState][evnt] = destState;
    }
    public void SendEvent(TE evnt)
    {
        if (matrix[currentState].ContainsKey(evnt))
            currentState = matrix[currentState][evnt];
    }
    public TS getState()
    {
        return currentState;
    }
    public void setCurrent(TS state)
    {
        currentState = state;
    }
}

