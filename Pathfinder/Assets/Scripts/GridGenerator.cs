﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class GridGenerator : MonoBehaviour
{
    public LayerMask unwalkableMask;
    public GameObject gridBluePrint;
    public GameObject nodeTile;

    protected Vector2 gridWorldSize;
    protected float nodeRadius;
    private int nodesInstantiated;
    Node[,] grid;

    float nodeDiameter;
    int gridSizeX, gridSizeY;

    void Awake()
    {
        nodesInstantiated = 0;//Just in case

        nodeRadius=nodeTile.GetComponent<SphereCollider>().radius;

        gridWorldSize.x = gridBluePrint.GetComponent<Transform>().lossyScale.x;
        gridWorldSize.y = gridBluePrint.GetComponent<Transform>().lossyScale.y;      

        nodeDiameter = nodeRadius * 2;
        gridSizeX = Mathf.RoundToInt(gridWorldSize.x / nodeDiameter);
        gridSizeY = Mathf.RoundToInt(gridWorldSize.y / nodeDiameter);
        CreateGrid();
    }

    void Update()
    {
        DrawTile();
    }

    void CreateGrid()
    {
        grid = new Node[gridSizeX, gridSizeY];
        Vector3 worldBottomLeft = transform.position - Vector3.right * gridWorldSize.x / 2 - Vector3.forward * gridWorldSize.y / 2;

        for (int x = 0; x < gridSizeX; x++)
        {
            for (int y = 0; y < gridSizeY; y++)
            {
                Vector3 worldPoint = worldBottomLeft + Vector3.right * (x * nodeDiameter + nodeRadius) + Vector3.forward * (y * nodeDiameter + nodeRadius);
                bool walkable = !(Physics.CheckSphere(worldPoint, nodeRadius, unwalkableMask));
                grid[x, y] = new Node(walkable, worldPoint, x , y);
            }
        }
    }

    public List<Node> GetNeighbours(Node node)
    {
        List<Node> neighbours = new List<Node>();

        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                if (x == 0 && y == 0)
                    continue;

                int checkX = node.gridX + x;
                int checkY = node.gridY + y;

                if (checkX >= 0 && checkX < gridSizeX && checkY >= 0 && checkY < gridSizeY)
                {
                    neighbours.Add(grid[checkX, checkY]);
                }
            }
        }

        return neighbours;
    }


    public Node NodeFromWorldPoint(Vector3 worldPosition)
    {
        float percentX = (worldPosition.x + gridWorldSize.x / 2) / gridWorldSize.x;
        float percentY = (worldPosition.z + gridWorldSize.y / 2) / gridWorldSize.y;
        percentX = Mathf.Clamp01(percentX);
        percentY = Mathf.Clamp01(percentY);

        int x = Mathf.RoundToInt((gridSizeX - 1) * percentX);
        int y = Mathf.RoundToInt((gridSizeY - 1) * percentY);
        return grid[x, y];
    }

    public List<Node> path;
    void DrawTile()
    {
        if (grid != null)
        {
            foreach (Node n in grid)
            {
                nodeTile.GetComponent<Renderer>().sharedMaterial.color = (n.walkable) ? Color.blue : Color.red;

                if (path != null)
                {
                    if (path.Contains(n))
                    {
                        //nodeTile.GetComponent<Renderer>().sharedMaterial.color = Color.green;
                        if (nodesInstantiated<=path.Count)
                        {
                            nodesInstantiated++;
                            Instantiate(nodeTile, n.worldPosition, Quaternion.Euler(90, 0, 0));
                        }
                    }
                }                
                if (nodesInstantiated<=grid.Length)
                {
                    //nodesInstantiated++;
                    //Instantiate(nodeTile, n.worldPosition, Quaternion.Euler(90, 0, 0));
                }
            }
        }
    }
}