﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mine:MonoBehaviour
{
    public int goldInMine;
    public float timeUntilMoreGold;
    private float timerGold;

    void Update()
    {
        timerGold -= Time.deltaTime;

        if (timerGold<=0)
        {
            goldInMine++;
            timerGold += timeUntilMoreGold;
        }
    }

    public int GetGold()
    {
        return goldInMine;
    }
}
