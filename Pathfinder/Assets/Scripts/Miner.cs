﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Miner : MonoBehaviour
{
    FSM<States, Events> minerFSM;

    private int goldCarrying;
    private int goldDeposited;
    public int backpackCapacity;
    public float speed;
    //public float minerDelay;

    public GameObject mine;
    public GameObject gridGenerator;

    private Mine mine2Check;
    private GridGenerator grid;
    private int nodeBeingChecked;

    public States currentState;

    public enum States
    {        
        Mining,
        Unloading,
        Checking,
        Traveling2Mine,
        Traveling2Deposit
    }

    public enum Events
    {        
        ReachMine,
        FinishDeposit,
        FinishMining,
        CheckMine,
        ReachDeposit
    }

    void Start ()
    {
        mine2Check = mine.GetComponent<Mine>();
        grid = gridGenerator.GetComponent<GridGenerator>();

        goldCarrying = 0;

        minerFSM = new FSM<States, Events>();

        minerFSM.setCurrent(States.Checking);

        minerFSM.SetRelation(States.Unloading, Events.FinishDeposit, States.Checking);
        minerFSM.SetRelation(States.Checking, Events.CheckMine, States.Traveling2Mine);
        minerFSM.SetRelation(States.Traveling2Mine, Events.ReachMine, States.Mining);
        minerFSM.SetRelation(States.Mining, Events.FinishMining, States.Traveling2Deposit);
        minerFSM.SetRelation(States.Traveling2Deposit, Events.ReachDeposit, States.Unloading);

    }

    void Update ()
    {
        currentState = minerFSM.getState();

        switch (minerFSM.getState())
        {
            case States.Mining:
                Mining();
                break;
            case States.Unloading:
                Unloading();
                break;
            case States.Checking:
                Checking();
                break;
            case States.Traveling2Mine:
                Travelling2Mine();
                break;
            case States.Traveling2Deposit:
                Travelling2Deposit();
                break;            
        }
    }

    void Unloading()
    {
        if (goldCarrying>0)
        {
            goldDeposited += goldCarrying;
            goldCarrying = 0;
            minerFSM.SendEvent(Events.FinishDeposit);
        }
    }

    void Checking()
    {
        if (mine2Check.goldInMine<=0)
        {
            return;
        }

        else
        {
            minerFSM.SendEvent(Events.CheckMine);          
        }
    }

    void Mining()
    {
        for (int i = 0; i < backpackCapacity; i++)
        {
            if (goldCarrying==backpackCapacity || mine2Check.goldInMine <= 0)
            {
                minerFSM.SendEvent(Events.FinishMining);               
            }

            else
            {
                goldCarrying++;
                mine2Check.goldInMine--;                                
            }            
        }
    }

    void Travelling2Mine()
    {
        if (this.GetComponent<Transform>().position!=grid.path[nodeBeingChecked].worldPosition)
        {
            this.GetComponent<Transform>().position = Vector3.MoveTowards(this.GetComponent<Transform>().position, grid.path[nodeBeingChecked].worldPosition, speed * Time.deltaTime);
        }
        else
        {
            nodeBeingChecked++;
            if (nodeBeingChecked==grid.path.Count-1)
            {
                minerFSM.SendEvent(Events.ReachMine);
            }
        }
    }

    void Travelling2Deposit()
    {
        if (this.GetComponent<Transform>().position != grid.path[nodeBeingChecked].worldPosition)
        {
            this.GetComponent<Transform>().position = Vector3.MoveTowards(this.GetComponent<Transform>().position, grid.path[nodeBeingChecked].worldPosition, speed * Time.deltaTime);
        }
        else
        {
            nodeBeingChecked--;
            if (nodeBeingChecked == 0)
            {
                minerFSM.SendEvent(Events.ReachDeposit);
            }
        }
    }
}